global find_word
extern string_equals

section .text

;rdi - string, rsi - dict address
find_word:
	xor rax, rax
.L:
	cmp rsi, 0
	je .NOT_FOUND
	push rsi
	push rdi
	add rsi, 8
	call string_equals
	pop rdi
	pop rsi
	cmp rax, 1
	je .FOUND
	mov rsi, [rsi]
	jmp .L
.NOT_FOUND:
	xor rax, rax
	ret
.FOUND:
	mov rax, rsi
	ret

