NASM = nasm
FLAGS = -felf64 -i
LINKER = ld
LINKER_FLAGS = -o
OUT_NAME = run

.phony: all
all: main.o lib.o dict.o
	$(LINKER) $(LINKER_FLAGS) $(OUT_NAME) $+


%.o: %.asm
	$(NASM) $(FLAGS) $@ $<

.phony: clean
clean:
	rm -rf *.o $(OUT_NAME)
