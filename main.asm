%include "lib.inc"
extern find_word

%define BUF_SIZE 255
%define ERR_EX_CODE 1
section .bss
buffer: resb BUF_SIZE 

section .rodata
error_buffer_size: db "Input string out of buffer size, only 255 bytes available", 10, 0
error_key: db "No such key found", 10, 0

%include "words.inc"

section .text
global _start
_start:
	mov rdi, buffer
	mov rsi, BUF_SIZE
	call read_word
	test rax, rax
	je .BUFFER_OUT
	mov rsi, next_element
	mov rdi, buffer
	call find_word
	cmp rax, 0
	je .KEY_NOT_FOUND
	mov rdi, rax
	add rdi, 8
	push rdi
	call string_length
	pop rdi
	add rax, rdi
	inc rax
	mov rdi, rax
	call print_string
	call print_newline
	xor rdi, rdi
	call exit
.KEY_NOT_FOUND:
	mov rdi, error_key
	call print_error
	mov rdi, ERR_EX_CODE
	call exit
.BUFFER_OUT:
	mov rdi, error_buffer_size
	call print_error
	mov rdi, ERR_EX_CODE
    call exit

